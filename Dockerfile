FROM cyancor/ubuntu-git:latest

LABEL maintainer="CyanCor GmbH - https://cyancor.com/"

RUN apt-get update && apt-get install --yes cmake g++
